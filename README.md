# Infrastructure

This repository contains files for setting up the infrastructure needed for the repository crawler and repository recommender.

The provided `docker-compose.yml` file can be used to start these services:

* `db`: A PostgreSQL database.
* `redis`: A Redis database.
* `pgadmin4`: A tool to manage the PostgreSQL database using a web interface.
* `reverse-proxy`: A NGINX reverse proxy that routes requests to the respective container. One exemplary configuration can be found in the file `nginx.conf`.

Configuration options can be provided using environment variables:

* `POSTGRES_DB`: The PostgreSQL database.
* `POSTGRES_USER`: The PostgreSQL user.
* `POSTGRES_PASSWORD`: The PostgreSQL password.
* `DB_VOLUME`: A folder where the data of the PostgreSQL database is saved to.
* `REDIS_VOLUME`: A folder where the data of the Redis database is saved to.
* `PGADMIN_DEFAULT_EMAIL`: E-Mail of the pgAdmin user.
* `PGADMIN_DEFAULT_PASSWORD`: Password of the pgAdmin user.
* `PGADMIN_VOLUME`: A folder where the data of pgAdmin is saved to.
* `SSL_CERT`, `SSL_KEY`: Paths to a certificate and a private key used for SSL/TLS.
* `BACKEND_NET`: The name of a Docker network to use for backend services.
* `REVERSE_PROXY_NET`: The name of a Docker network to use for proxied services.
* `FRONTEND_NET`: The name of a Docker network to use for frontend services.
